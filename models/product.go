package models

type Product struct {
	ID                int64       `json:"id" bson:"id"`
	AlternativeID     *int64      `json:"alternative_id" bson:"alternative_id"`
	SKU               string      `json:"sku" bson:"sku"`
	Name              string      `json:"name" bson:"name"`
	Brand             string      `json:"brand" bson:"brand"`
	CustomReferenceID *string     `json:"custom_reference_id" bson:"custom_reference_id"`
	DefaultImageURL   *string     `json:"default_image_url" bson:"default_image_url"`
	DescriptionText   *string     `json:"description_text" bson:"description_text"`
	DescriptionHTML   *string     `json:"description_html" bson:"description_html"`
	ShortSummary      *string     `json:"short_summary" bson:"short_summary"`
	Enabled           bool        `json:"enabled" bson:"enabled"`
	Images            []string    `json:"images" bson:"images"`
	Categories        []string    `json:"categories"`
	Variations        []Variation `json:"variations"`
}

type Variation struct {
	Type  string `json:"type" bson:"type"`
	Label string `json:"label" bson:"label"`
	Value string `json:"value" bson:"value"`
}
