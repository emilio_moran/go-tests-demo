package services

import (
	"fmt"

	"bitbucket.org/emilio_moran/go-tests-demo/models"
)

type DB interface {
	FindProductBySKU(string) (*models.Product, error)
}

type ProductService struct {
	db DB
}

func NewProductService(db DB) *ProductService {
	return &ProductService{
		db: db,
	}
}

func (ps *ProductService) CreateProduct(p *models.Product) error {
	if p == nil {
		return nil
	}

	if p.SKU == "" {
		return fmt.Errorf("invalid sku")
	}

	if p.Name == "" {
		return fmt.Errorf("invalid name")
	}

	if p.Brand == "" {
		return fmt.Errorf("invalid brand")
	}

	// create product code ...

	return nil
}

func (ps *ProductService) FindProductBySKU(sku string) (*models.Product, error) {
	product, err := ps.db.FindProductBySKU(sku)
	if err != nil {
		return nil, err
	}

	return product, nil
}
