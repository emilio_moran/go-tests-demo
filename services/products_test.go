package services

import (
	"fmt"
	"testing"

	"bitbucket.org/emilio_moran/go-tests-demo/models"
	tb "bitbucket.org/emilio_moran/go-tests-demo/testbuilders"
	"github.com/stretchr/testify/assert"
)

var (
	dbMock = &DBMock{}
)

type DBMock struct {
	Err     error
	Product *models.Product
}

func (m *DBMock) FindProductBySKU(string) (*models.Product, error) {
	if m.Err != nil {
		return nil, m.Err
	}

	return m.Product, nil
}

func TestCreateProduct(t *testing.T) {
	productService := NewProductService(dbMock)

	tests := []struct {
		name    string
		product *models.Product
		wantErr bool
	}{
		{
			name:    "when a product is valid should be return nil",
			product: tb.BuildProduct(),
			wantErr: false,
		},
		{
			name:    "when sku is empty should be return error",
			product: tb.BuildProduct(tb.WithSKU("")),
			wantErr: true,
		},
		{
			name:    "when name is empty should be return error",
			product: tb.BuildProduct(tb.WithName("")),
			wantErr: true,
		},
		{
			name:    "when sku is empty should be return error",
			product: tb.BuildProduct(tb.WithBrand("")),
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if err := productService.CreateProduct(tt.product); (err != nil) != tt.wantErr {
				t.Errorf("error = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}

func TestFindProductBySKU(t *testing.T) {
	productService := NewProductService(dbMock)

	tests := []struct {
		name    string
		sku     string
		product *models.Product
		err     error
		wantErr bool
	}{
		{
			name:    "when db function return an error should be return error",
			sku:     "abc",
			product: nil,
			err:     fmt.Errorf("error in db"),
			wantErr: true,
		},
		{
			name:    "when product not found should be return nil",
			sku:     "abc",
			product: nil,
			err:     nil,
			wantErr: false,
		},
		{
			name:    "when product is found should be return the product",
			sku:     "abc",
			product: tb.BuildProduct(tb.WithSKU("abc")),
			err:     nil,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {

			dbMock.Err = tt.err
			dbMock.Product = tt.product

			product, err := productService.FindProductBySKU(tt.sku)
			if !tt.wantErr {
				if assert.NoError(t, err) {
					assert.Equal(t, tt.product, product)
				}
			} else {
				if assert.Error(t, err) {
					assert.Nil(t, product)
				}
			}
		})
	}
}
