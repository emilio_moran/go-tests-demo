package testbuilders

import "bitbucket.org/emilio_moran/go-tests-demo/models"

type productOption func(*models.Product)
type variationOption func(*models.Variation)

func BuildVariation(options ...variationOption) *models.Variation {
	v := &models.Variation{
		Type:  "CUSTOM",
		Label: "Color",
		Value: "RED",
	}

	for _, option := range options {
		option(v)
	}

	return v
}

func WithType(t string) variationOption {
	return func(v *models.Variation) {
		v.Type = t
	}
}

func WithLabel(label string) variationOption {
	return func(v *models.Variation) {
		v.Label = label
	}
}

func WithValue(value string) variationOption {
	return func(v *models.Variation) {
		v.Value = value
	}
}

func BuildProduct(options ...productOption) *models.Product {
	p := &models.Product{
		ID:                1,
		AlternativeID:     nil,
		SKU:               "ECN300",
		Name:              "PS5",
		Brand:             "SONY",
		CustomReferenceID: BuildPointerString("ABC001"),
		DefaultImageURL:   BuildPointerString(BuildImageURL("")),
		DescriptionText:   BuildPointerString("New console videogame"),
		DescriptionHTML:   nil,
		ShortSummary:      nil,
	}

	for _, option := range options {
		option(p)
	}

	return p
}

func WithSKU(sku string) productOption {
	return func(p *models.Product) {
		p.SKU = sku
	}
}

func WithName(name string) productOption {
	return func(p *models.Product) {
		p.Name = name
	}
}

func WithBrand(brand string) productOption {
	return func(p *models.Product) {
		p.Brand = brand
	}
}

func WithVariations(v []models.Variation) productOption {
	return func(p *models.Product) {
		p.Variations = v
	}
}
