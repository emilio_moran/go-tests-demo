package testbuilders

func BuildPointerString(v string) *string {
	return &v
}

func BuildImageURL(ext string) string {
	extImage := "png"
	if ext != "" {
		extImage = ext
	}

	return "https://public-image." + extImage
}
