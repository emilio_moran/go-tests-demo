module bitbucket.org/emilio_moran/go-tests-demo

go 1.16

require (
	github.com/fatih/color v1.12.0 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/rakyll/gotest v0.0.6 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
